#ifndef PLAYER_H    // To make sure you don't declare the function more than once by including the header multiple times.
#define PLAYER_H

#include "opencv2\highgui\highgui.hpp"
#include "opencv2\opencv.hpp"
#include "opencv2\imgproc\imgproc.hpp"

using namespace cv;
using namespace std;
using namespace ml;

Mat plasmaAndCitra(Mat citraInput, int plasmaH0, int plasmaH1, int plasmaS0, int plasmaS1, int plasmaV0, int plasmaV1);
Mat intiAndCitra(int intiH0, int intiH1, int intiS0, int intiS1, int intiV0, int intiV1);
Mat openingInti(Mat citraInput, Mat andInti, int ukuranInti);
Mat labelPlasma(Mat citraInput, Mat andPlasma);
Mat labelInti(Mat citraInput, Mat andInti);

#endif