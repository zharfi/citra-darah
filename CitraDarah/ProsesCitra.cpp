#include "ProsesCitra.h"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\opencv.hpp"
#include "opencv2\imgproc\imgproc.hpp"

using namespace cv;
using namespace std;
using namespace ml;

//Variabel Global
Mat citraH, citraS, citraV;

//buat plasma
struct segmen {
	bool darahputih;
	int x1, y1, x2, y2, xc, yc;
	std::vector<cv::Point2i> titik;
};

Mat plasmaAndCitra(Mat citraInput, int plasmaH0, int plasmaH1, int plasmaS0, int plasmaS1, int plasmaV0, int plasmaV1) {
	//konversi RGB ke HSV dan split ke 3-channels HSV
	Mat konversi, citraHSV[3], plasmaAnd;
	if (citraInput.data == 0)
		return konversi;
	cvtColor(citraInput, konversi, cv::COLOR_BGR2HSV); /*perubahan CV_BGR2HSV ke cv::COLOR_BGR2HSV*/
	split(konversi, citraHSV);
	citraH = citraHSV[0];
	citraS = citraHSV[1];
	citraV = citraHSV[2];

	//deklarasi pengambangan plasma
	Mat ambangHplasma, ambangSplasma, ambangVplasma, ambangTempInti, plasmaTerambang;
	
	//pengambangan plasma
	if (plasmaH0 < plasmaH1)
		inRange(citraH, cv::Scalar(plasmaH0), cv::Scalar(plasmaH1), ambangHplasma);
	else {
		inRange(citraH, cv::Scalar(plasmaH1), cv::Scalar(plasmaH0), ambangHplasma);
		bitwise_not(ambangHplasma, ambangHplasma);
	}
	if (plasmaS0 < plasmaS1)
		inRange(citraS, cv::Scalar(plasmaS0), cv::Scalar(plasmaS1), ambangSplasma);
	else {
		inRange(citraS, cv::Scalar(plasmaS1), cv::Scalar(plasmaS0), ambangSplasma);
		bitwise_not(ambangSplasma, ambangSplasma);
	}
	if (plasmaV0 < plasmaV1)
		inRange(citraV, cv::Scalar(plasmaV0), cv::Scalar(plasmaV1), ambangVplasma);
	else {
		inRange(citraV, cv::Scalar(plasmaV1), cv::Scalar(plasmaV0), ambangVplasma);
		bitwise_not(ambangVplasma, ambangVplasma);
	}

	//penjumlahan hasil pengambangan
	bitwise_and(ambangHplasma, ambangSplasma, plasmaAnd);
	bitwise_and(plasmaAnd, ambangVplasma, plasmaAnd);
	citraInput.copyTo(plasmaTerambang, plasmaAnd);

	return plasmaAnd;
}

Mat intiAndCitra(int intiH0, int intiH1, int intiS0, int intiS1, int intiV0, int intiV1, int ukuranInti) {
	//deklarasi pengambangan inti
	Mat ambangHinti, ambangSinti, ambangVinti, intiAnd;

	//pengambangan inti sel
	if (intiH0 < intiH1)
		inRange(citraH, cv::Scalar(intiH0), cv::Scalar(intiH1), ambangHinti);
	else {
		inRange(citraH, cv::Scalar(intiH1), cv::Scalar(intiH0), ambangHinti);
		bitwise_not(ambangHinti, ambangHinti);
	}
	if (intiS0 < intiS1)
		inRange(citraS, cv::Scalar(intiS0), cv::Scalar(intiS1), ambangSinti);
	else {
		inRange(citraS, cv::Scalar(intiS1), cv::Scalar(intiS0), ambangSinti);
		bitwise_not(ambangSinti, ambangSinti);
	}
	if (intiV0 < intiV1)
		inRange(citraV, cv::Scalar(intiV0), cv::Scalar(intiV1), ambangVinti);
	else {
		inRange(citraV, cv::Scalar(intiV1), cv::Scalar(intiV0), ambangVinti);
		bitwise_not(ambangVinti, ambangVinti);
	}
	bitwise_and(ambangHinti, ambangSinti, intiAnd);
	bitwise_and(intiAnd, ambangVinti, intiAnd);

	Mat element;
	element = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(2 * ukuranInti + 1, 2 * ukuranInti + 1), cv::Point(ukuranInti, ukuranInti));
	erode(intiAnd, intiAnd, element);
	dilate(intiAnd, intiAnd, element);
	dilate(intiAnd, intiAnd, element);
	erode(intiAnd, intiAnd, element);

	return intiAnd;
}

Mat openingInti(Mat citraInput, Mat intiAnd) {
	Mat intiTerambang;
	citraInput.copyTo(intiTerambang, intiAnd);	
	return intiTerambang;
}

Mat labelPlasma(Mat citraInput, Mat andPlasma) {
	//TODO Buat fungsi label Plasma
	std::vector<segmen> blobSel;
	Mat plasmaANDbiner;
	plasmaANDbiner = andPlasma / 255;
	blobSel.clear();
	Mat label_image;
	plasmaANDbiner.convertTo(label_image, CV_32FC1);
	int ukuranMinimalSelplasma = 2000;
	int label_count = 2;
	for (int y = 0; y<plasmaANDbiner.rows; y++) { //pemberian label satu per satu pp piksel warna putih
		for (int x = 0; x<plasmaANDbiner.cols; x++) {
			if ((int)label_image.at<float>(y, x) != 1)
				continue;
			cv::Rect rect; //kotak
			cv::floodFill(label_image, cv::Point(x, y), cv::Scalar(label_count), &rect, cv::Scalar(0), cv::Scalar(0), 4);
			segmen blob; //fungsi blob sesuai segmen, dalam lingkup struct
			blob.darahputih = false; //dari program di atas, maka blob darah putih adalah false
			int xmin = 10000, ymin = 10000, xmaks = 0, ymaks = 0; //scanning menentukan blob
			for (int i = rect.y; i<(rect.y + rect.height); i++) {
				for (int j = rect.x; j<(rect.x + rect.width); j++) {
					if ((int)label_image.at<float>(i, j) != label_count)
						continue;
					blob.titik.push_back(cv::Point2i(j, i));
					if (j<xmin)
						xmin = j;
					if (j>xmaks)
						xmaks = j;
					if (i<ymin)
						ymin = i;
					if (i>ymaks)
						ymaks = i;
				}
			}
			if (blob.titik.size()>ukuranMinimalSelplasma) {
				blob.x1 = xmin;
				blob.x2 = xmaks;
				blob.y1 = ymin;
				blob.y2 = ymaks;
				blobSel.push_back(blob);

				label_count++;
			}
		}
	}
	//buat citra label plasma
	Mat plasmaLabelSel = Mat::zeros(citraInput.size(), CV_8UC3);
	for (size_t i = 0; i<blobSel.size(); i++) {
		unsigned char r = 255 * (rand() / (1.0 + RAND_MAX));
		unsigned char g = 255 * (rand() / (1.0 + RAND_MAX));
		unsigned char b = 255 * (rand() / (1.0 + RAND_MAX));
		for (size_t j = 0; j<blobSel[i].titik.size(); j++) {
			int x = blobSel[i].titik[j].x;
			int y = blobSel[i].titik[j].y;
			plasmaLabelSel.at<cv::Vec3b>(y, x)[0] = b;
			plasmaLabelSel.at<cv::Vec3b>(y, x)[1] = g;
			plasmaLabelSel.at<cv::Vec3b>(y, x)[2] = r;
		}
	}
	return plasmaLabelSel;
}

Mat labelInti(Mat citraInput, Mat intiAnd) {
	//TODO buat fungsi label inti
	Mat tes;
	return tes;
}
